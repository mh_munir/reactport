import Sidebar from "./components/Sidebar";
import styled from "styled-components";
import HomePage from "./pages/HomePage";
import AboutPage from "./pages/AboutPage";
import ResumePage from "./pages/ResumePage";
import PortfolioPage from "./pages/PortfolioPage";
import BlogPage from "./pages/BlogPage";
import ContactPage from "./pages/ContactPage";
import { Route, Switch } from "react-router-dom";

function App() {
  return (
    <div className="App">
      <Sidebar/>
      <MainContentStyled>
       <div className="lines">
          <span></span>
          <span></span>
          <span></span>
          <span></span>
          <span></span>
       </div>

       <Switch>
          <Route path="/" exact><HomePage/></Route>
          <Route path="/about" exact><AboutPage/></Route>
          <Route path="/resume" exact><ResumePage/></Route>
          <Route path="/portfolios" exact><PortfolioPage/></Route>
          <Route path="/blogs" exact><BlogPage/></Route>
          <Route path="/contact" exact><ContactPage/></Route>
       </Switch>
      </MainContentStyled>
    </div>
  );
}

const MainContentStyled = styled.main`
    position: relative;
    width: calc(100% - 260px);
    margin-left:260px;
    min-height: 100vh;
    .lines{
      position: absolute;
      top: 0;
      height: 100%;
      min-height: 100vh
      span{
        display: inline-block;
        left: 0;
        width: 1px;
        background: rgba(46,52,78,.3);
        height: 100%;
      }
    }
`;

export default App;
