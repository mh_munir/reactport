import React from 'react';
import styled from 'styled-components';
import Particle from '../components/Particle';
import FacebookIcon from '@material-ui/icons/Facebook';
import TwitterIcon from '@material-ui/icons/Twitter';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import EmailIcon from '@material-ui/icons/Email';

 function HomePage() {
  return (
    <HomePageStyled>
        <div className="p-particles-js">
            <Particle/>
        </div>
        <div className="typography">
            <h1> Hi I'm <span>Mh Munir</span></h1>
            <p>Lorem Ipsum is simply dummy text of the printing and 
                typesetting industry. Lorem Ipsum has been the industry's standard dummy
            </p>
            <div className="icons">
                <div className="icon i-facebook">
                    <FacebookIcon />
                </div>
                <div className="icon i-twitter">
                    <TwitterIcon/>
                </div>
                <div className="icon i-linkdin">
                    <LinkedInIcon />
                </div>
                <div className="icon i-gmail">
                    <EmailIcon />
                </div>
            </div>
        </div>
    </HomePageStyled>
  );
}

const HomePageStyled = styled.section`
    position: relative;
    width: 100%;
    height: 100vh;
    .p-particles-js{
        position: absolute;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        right: 0;
        bottom:0;
    }
    .typography{
        position: absolute;
        width: 60%;
        top: 50%;
        left: 50%;
        transform: translate(-50%,-50%);
        text-align: center;
        z-index: 1;
        .icons{
            display: flex;
            flex-direction: row;
            justify-content: center;
            margin-top: 1rem;
            .icon{
                border: 2px solid var(--border-color);
                display: flex;
                justify-content: center;
                align-items: center;
                padding: .3rem;
                border-radius: 50%;
                cursor: pointer;
                transition: all .5s linear;
                &:hover{
                    border: 2px solid var(--primary-color);
                    background-color: var(--primary-color);
                }
                &:not(:last-child){
                    margin-right: .5rem;
                }
                svg{
                    font-size: 1.2rem;
                    color: var(--font-light-color);
                    transition: all .5s linear;
                }
                &:hover>svg{
                   color: var(--white-color);
                }
            }
        }
        p{
            font-size: 1rem;
            line-height: 1.5;
            font-weight: 300;
        }
    }


`;

export default HomePage;