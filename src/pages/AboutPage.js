import React from 'react';
import styled from 'styled-components';
import {MainLayout} from '../../src/styles/Layouts'
import Title from '../components/Title';
import AboutInfoSection from '../components/AboutInfoSection'

function AboutPage() {
  return (

    <MainLayout>
      <AboutStyled>
        <Title title={'About Me'} span={'About Me'}/>
        <AboutInfoSection/>
      </AboutStyled>
    </MainLayout>
  );
}

const AboutStyled = styled.section`



`;

export default AboutPage;