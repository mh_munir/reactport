import React from 'react';
import styled from 'styled-components';

function PrimaryButton({title, href}) {
  return (
    <PrimaryButtonStyled href={href}>
      {title}
    </PrimaryButtonStyled>
  );
}

const PrimaryButtonStyled = styled.a`
    background-color: var(--primary-color);
    position: relative;
    padding: 0 30px;
    color: #fff;
    border: 0;
    display: inline-block;
    z-index: 1;
    text-transform: uppercase;
    font-size: .8rem;
    letter-spacing: 1px;
    height: 50px;
    line-height: 50px;
    margin-top: 15px;

    &::before,
    &::after{
      content: "";
      position: absolute;
      width: 0;
      height: 2px;
      background: var(--white-color);
      transition: all .5s ease-in-out;
    }
    &::before{
      top: 0;
      right: 0;
    }
    &::after{
      left: 0;
      bottom: 0;
    }
    &:hover::before,
    &:hover::after{
      width: 100%;
    }
`;

export default PrimaryButton;
