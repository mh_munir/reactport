import React from 'react';
import styled from 'styled-components';

 function Title({title, span}) {
  return (
    <TitleStyled>
        <h2>{title}</h2>
        <span>{span}</span>
    </TitleStyled>
  );
}

const TitleStyled = styled.div`
    position: relative;
    display:flex;
    flex-direction: column;
    display : inline-block;
    margin-bottom: 30px;
    h2{
        position:relative;
        font-size: 2rem;
        color: var(--white-color);
        text-transform: uppercase;
        line-height: .5;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
        font-kerning: none;
        letter-spacing: 0.5px;
        &::before, 
        &::after{
            content: "";
            position: absolute;
            height: 5px;
            left: 0;
            bottom: -25px;
            background-color: var(--primary-color);
            border-radius: 20px;
        }
        &::before{
            width: 120px;
            opacity: .3;
        }
        &::after{
            width: 50px;
        }
    }
    span{
        font-size: 3rem;
        line-height: 1.1;
        font-weight: 900;
        text-transform: uppercase;
        -webkit-text-stroke-width: 1px;
        -webkit-text-stroke-color: var(--primary-color);
        -webkit-text-fill-color: transparent;
        opacity: .2;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
        font-kerning: none;
        letter-spacing: 0.5px;
    }

`;
export default Title;