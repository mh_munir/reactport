import React from 'react';
import styled from 'styled-components';
import Aboutme from '../images/about.jpg'
import PrimaryButton from './PrimaryButton';

function AboutInfoSection() {
  return (
    <AboutInfoStyled>
        <div className="about-left">
            <img src={Aboutme} alt="About Me" className="about-img"/>
        </div>
        <div className="about-right">
            <div className="sub-title">
                <h4>I'm <span>Munir Hossain</span> </h4>
            </div>
            <div className="paragraphy">
                <p>I am a frontend web developer. I can provide clean code 
                    and pixel perfect design. I also make website more & 
                    more interactive with web animations.</p>
            </div>
            <div className="about-info">
               <ul>
                   <li><label>Full Name</label><span>:</span>Munir Hossain</li>
                   <li><label>Age</label><span>:</span>26 Years Old</li>
                   <li><label>Nationality</label><span>:</span>Bangladesh</li>
                   <li><label>Languages</label><span>:</span>Bengali, English</li>
                   <li><label>Adress</label><span>:</span>Chawk Bazar, Chittagong</li>
                   <li><label>Study</label><span>:</span>Diploma In Computer</li>
               </ul>
            </div>
            <PrimaryButton href={"/home"} title={'Download Cv'}/>
        </div>
    </AboutInfoStyled>
  );
}

const AboutInfoStyled = styled.div`
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-right: -15px;
    margin-left: -15px;
    .about-left{
        flex: 0 0 50%;
        max-width: 50%;
        position: relative;
        padding-right: 15px;
        padding-left: 15px;
        .about-img{
            width: 100%;
            max-height: 400px;
            object-fit: cover;
            object-position: top;
        }
    }
    .about-right{
        flex: 0 0 50%;
        max-width: 50%;
        position: relative;
        padding-right: 15px;
        padding-left: 15px;
        .sub-title{
            h4{
                color: var(--white-color);
                font-size: 1.5rem;
                font-weight: 600;
                line-height: 1.5;
                text-transform: capitalize;
                margin-top: -8px;
                margin-bottom: 7px;
                span{
                    color: var(--primary-color);
                    font-size: 1.5rem;
                    font-weight: 600;
                    line-height: 1.5;
                    text-transform: capitalize;
                }
            }
        }
        .paragraphy{
            margin-bottom: 15px;
            p{
                font-size: .95rem;
                font-weight: 400;
                line-height: 1.5;
                color: var(--font-light-color);
            }
        }
        .about-info{
            ul{
                li{
                    color: var(--font-light-color);
                    font-size: .9rem;
                    line-height: 1.5;
                    font-weight:400;
                    label{
                        display: inline-block;
                        min-width: 100px;
                        font-size: .9rem;
                        font-weight:700;
                        line-height: 1.5;
                    }
                    span{
                        display: inline-block;
                        min-width: 20px;
                    }
                }
            }
        }
    }

`;


export default AboutInfoSection;
