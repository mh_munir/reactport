import React from 'react';
import styled from 'styled-components';
import {NavLink} from 'react-router-dom';
import avatar from '../images/avatar.jpg'

function Navigation() {
  return (
    <NavigationStyle>
        <div className="avatar">
            <img src={avatar} alt="Avatar" className="avatar__img"/>
        </div>
        <ul className="nav-items">
            <li className="nav-item"> <NavLink to="/" activeClassName="active-class" exact>Home</NavLink> </li>
            <li className="nav-item"> <NavLink to="/about" activeClassName="active-class" exact>About</NavLink> </li>
            <li className="nav-item"> <NavLink to="/resume" activeClassName="active-class" exact>Resume</NavLink> </li>
            <li className="nav-item"> <NavLink to="/portfolios" activeClassName="active-class" exact>Portfolios</NavLink> </li>
            <li className="nav-item"> <NavLink to="/blogs" activeClassName="active-class" exact>Blogs</NavLink> </li>
            <li className="nav-item"> <NavLink to="/contact" activeClassName="active-class" exact>Contact</NavLink> </li>
        </ul>
        <footer className="footer">
            <p>@2021 My Portfolio.</p>
        </footer>
    </NavigationStyle>
  );
}

const NavigationStyle =styled.nav`
    position: relative;
    display: flex;
    flex-direction: column;
    flex-wrap: nowrap;
    justify-content: space-between;
    align-items: center;
    width: 100%;
    height: 100%;
    border-right: 1px solid var(--border-color);

    .avatar{
        width:100%;
        text-align: center;
        border-bottom: 1px solid var(--border-color);
        padding: 1rem 0;
        
    }
    .avatar__img{
        width: 200px;
        height: 200px;
        border-radius: 50%;
        object-fit: cover;
        object-position: center;
        border: 8px solid var(--border-color);
    }
    .nav-items{
        width: 100%;
        text-align: center;
        .active-class{
            background-color: var(--primary-color);
        } 

        li{
            display: block;
            a{
                position: relative;
                display: block;
                font-size: .94rem;
                font-weight: 600;
                padding: .2rem 0;
                width: 100%;
                overflow: hidden;
                letter-spacing: .5px;
                text-transform: capitalize;
                &:hover{
                    cursor: pointer;
                }
                &::before{
                    content: "";
                    position: absolute;
                    bottom: 0;
                    left: 0;
                    width: 0;
                    height: 50%;
                    background-color: var(--primary-color);
                    transition: all .4s cubic-bezier(.99,-0.18,.26,.96);
                    transform-origin: left bottom;
                    opacity: .21;
                    z-index: -1;
                
                }
                &:hover::before{
                    width: 100%;
                    height: 100%;
                }
            }
        }
    }
    .footer{
        width: 100%;
        text-align: center;
        border-top: 1px solid var(--border-color);
        p{
            padding: 1rem 0;
            font-size: 1rem;
            text-transform: capitalize;
        }
    }

`;

export default Navigation;
